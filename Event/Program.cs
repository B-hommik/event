﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event
{
    class Program
    {
        static void Main(string[] args)
        {
            var thea = new Inimene { Nimi = "Thea", Vanus = 33 };
            thea.Pidu += x => Console.WriteLine("Nüüd saab pidu!!");
            for (; thea.Vanus++ < 100;) ;
        }
    }
    class Inimene
    {
        public event Action<Inimene> Pidu;
        public event Action<Inimene> Pension;
        private int vanus;
        public string Nimi { get; set; }
        public int Vanus
        {
            get => vanus;
            set
            {
                vanus = value;
                if (vanus % 25 == 0)
                    // if (Pidu != null) Pidu(this);
                    Pidu?.Invoke(this);
                if (vanus > 75) Pension?.Invoke(this);

            }
        }

        public override string ToString() => $"{Nimi} on {Vanus} aastane";
    }
}
